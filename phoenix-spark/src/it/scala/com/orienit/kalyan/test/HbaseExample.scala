package com.orienit.kalyan.test

import org.apache.spark._
import org.apache.spark.sql._
import org.apache.hadoop.io.LongWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.mapreduce.lib.output._
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat
import org.apache.hadoop.hbase.KeyValue
import scala.collection.mutable.TreeSet
import scala.collection.mutable.ListBuffer

object HbaseExample extends App {
  val sc = new SparkContext("local", "kalyan")
  val sqlContext = new SQLContext(sc)
  val conf = new Configuration()

  val key = sc.parallelize(List(1, 2, 3, 4))
  val value = sc.parallelize(List("a", "b", "c", "d"))
  val map = key.zip(value)

  var path = "hdfs://localhost:8020/hbase/data/default/sample/8b698405c72a9158f61014e5c617002c/cf"
  path = "file:///tmp/hbase/data/default/sample/8b698405c72a9158f61014e5c617002c/cf"

  val data = map.map(x => (new ImmutableBytesWritable(Bytes.toBytes(x._1.toString())), new KeyValue(Bytes.toBytes(x._1.toString()), Bytes.toBytes("cf"), Bytes.toBytes("a"), Bytes.toBytes(x._2))))
  data.saveAsNewAPIHadoopFile(path, classOf[ImmutableBytesWritable], classOf[KeyValue], classOf[HFileOutputFormat], conf);

  val mydata = sc.newAPIHadoopFile(path, classOf[HFileInputFormat], classOf[ImmutableBytesWritable], classOf[KeyValue], conf)

  println("------------------")

  // if possible optimize this to solve the row data . 
  // i.e prepare entire row (rowkey, cf:a, value)

  def convertToRow(data: Iterator[(ImmutableBytesWritable, KeyValue)]): Iterator[(String, String, String)] = {
    data.map(row => {
      val keyvalue = row._2
      val rowkey = Bytes.toStringBinary(keyvalue.getRow())
      val family = Bytes.toStringBinary(keyvalue.getFamily())
      val qualifier = Bytes.toStringBinary(keyvalue.getQualifier())
      val value = Bytes.toStringBinary(keyvalue.getValue)
      val column = family + ":" + qualifier
      (rowkey, column, value)
    })
  }

  val rowRDD = mydata.mapPartitions(convertToRow, true)
  rowRDD.foreach(println)

  println("------------------")

  var columns = rowRDD.groupBy(x => x._2).map(x => x._1).collect().sorted
  val cols = sc.broadcast(columns)
  val fullRow = rowRDD.groupBy(x => x._1).map(data => {
    val columns = cols.value
    val rowkey = data._1
    val map = data._2.map(t => (t._2, t._3)).toMap
    var row = new ListBuffer[String]()
    row += rowkey
    columns.foreach { column => row += map.getOrElse(column, "") }
    row.toList
  })
  fullRow.foreach(println)

  println("------------------")

  columns.foreach { println }

  println("------------------")

  println("i am done")
}


















