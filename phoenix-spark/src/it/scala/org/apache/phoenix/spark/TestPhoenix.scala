package org.apache.phoenix.spark

import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext

object TestPhoenix extends App {
  val sc = new SparkContext("local", "kalyan")
  val sqlContext = new SQLContext(sc)

  var df = sqlContext.load("org.apache.phoenix.spark", Map("table" -> "\"space\"", "zkUrl" -> "localhost:2181"))

  df.filter(df.col("first name").equalTo("xyz")).show
  df.filter(df.col("key").equalTo("key1")).show
  df.filter(df("key") === "key1").show()
  df.filter("key = 'key1'").show

  df = sqlContext.load("org.apache.phoenix.spark", Map("table" -> "\"small\"", "zkUrl" -> "localhost:2181"))

  df.filter(df.col("first name").equalTo("foo")).show
  df.filter("key = 'key1'").show
  df.filter("salary = '10000'").show
  df.filter("salary > '10000'").show

  df.registerTempTable("temp")
  sqlContext.sql("select * from temp where \"first name\" = 'foo' ").show
  sqlContext.sql("select * from temp where salary = '10000' ").show
  sqlContext.sql("select * from temp where salary > '10000' ").show
  sqlContext.sql("select * from temp where \"salary\" = '10000' ").show
}