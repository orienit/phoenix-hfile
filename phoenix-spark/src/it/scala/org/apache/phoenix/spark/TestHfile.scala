package org.apache.phoenix.spark

import org.apache.spark._
import org.apache.spark.sql._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.mapreduce.lib.output._
import org.apache.phoenix.spark.HFileConversions

object TestHfile extends App {
  val sc = new SparkContext("local", "kalyan")
  val sqlContext = new SQLContext(sc)
  val conf = new Configuration()
  var path = "hdfs://localhost:8020/hbase/data/default/sample/8b698405c72a9158f61014e5c617002c/cf"
  path = "hdfs://localhost:8020/hbase/data/default/STUDENT/e1e431ed13e8507c5e339f605f0fee71/0/bde62e8991584e5db3145171276f0317"
  path = "file:///tmp/hbase/data/default/sample/8b698405c72a9158f61014e5c617002c/cf"
  path = "hdfs://localhost:8020/hbase/data/default/test/1e2511a696df85bc1de1d5e082c97ed9/cf/4d4a3921b8e54cc29f07ba5d9cefb9f8"

  val zkUrl = Some("localhost:2181")

  val fieldsMapping = Map("rowkey" -> "rowkey", "cf:a" -> "0.c1", "cf:b" -> "0.c2", "cf:c" -> "0.c3")
  val schemaMapping = Map("rowkey" -> "PVarchar", "0.c1" -> "PVarchar", "0.c2" -> "PVarchar", "0.c3" -> "PVarchar")

  // val fieldsMapping = Map("rowkey" -> "rowkey", "cf:a" -> "0.c1")
  // val schemaMapping = Map("rowkey" -> "PInteger", "0.c1" -> "PVarchar")

  val sqlCon = new HFileConversions(sqlContext)

  println("-----------------------------------------------------")

  val hfileDF1 = sqlCon.hFileAsDataFrame(path, conf, null, null)
  val hfileDF11 = sqlCon.hFileAsDataFrame(path, conf)
  hfileDF1.printSchema()
  hfileDF1.show()
  hfileDF1.registerTempTable("test1")
  sqlContext.sql("select * from test1").show()

  hfileDF1.saveToPhoenix("\"mytable1\"", conf, zkUrl)

  println("-----------------------------------------------------")

  val hfileDF2 = sqlCon.hFileAsDataFrame(path, conf, fieldsMapping, null)
  hfileDF2.printSchema()
  hfileDF2.show()
  hfileDF2.registerTempTable("test2")
  sqlContext.sql("select * from test2").show()
  hfileDF2.saveToPhoenix("\"mytable2\"", conf, zkUrl)

  println("-----------------------------------------------------")

  val hfileDF3 = sqlCon.hFileAsDataFrame(path, conf, fieldsMapping, schemaMapping)
  hfileDF3.printSchema()
  hfileDF3.show()
  hfileDF3.registerTempTable("test3")
  sqlContext.sql("select * from test3").show()

  hfileDF3.saveToPhoenix("\"mytable3\"", conf, zkUrl)

  println("-----------------------------------------------------")

}