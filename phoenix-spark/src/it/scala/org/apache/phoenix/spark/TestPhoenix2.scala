package org.apache.phoenix.spark

import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.apache.hadoop.conf.Configuration

object TestPhoenix2 extends App {
  val sc = new SparkContext("local", "kalyan")
  val sqlContext = new SQLContext(sc)
  val configuration = new Configuration()

  val df1 = sqlContext.phoenixTableAsDataFrame("tbl_1", Array("CF1.C1", "CF2.C2"), conf = configuration)

  df1.show()

  val df2 = sqlContext.phoenixTableAsDataFrame("tbl_1", Array("CF1.C1", "CF2.C1"), conf = configuration)

  df2.show()

}