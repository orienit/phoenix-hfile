package org.apache.phoenix.spark

import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.phoenix.schema.types._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.KeyValue
import org.apache.spark.rdd.RDD
import java.lang.Long
import java.lang.Short
import java.lang.Double
import java.lang.Float
import java.sql.Timestamp
import java.sql.Date
import java.lang.Boolean

class HFileConversions(@transient val sqlContext: SQLContext) extends Serializable {

  def hFileAsDataFrame(path: String, conf: Configuration = new Configuration,
    fieldsMapping: Map[String, String] = null, schemaMapping: Map[String, String] = null): DataFrame = {
    val sc = sqlContext.sparkContext

    // prepare hFileRDD from `hfile` path
    val hFileRDD = sc.newAPIHadoopFile(path, classOf[HFileInputFormat], classOf[ImmutableBytesWritable], classOf[KeyValue], conf)

    // prepare rowRDD from hFileRDD
    val rowRDD = hFileRDD.mapPartitions(convertToRow, true)

    // get all the column names in sorted order from rowRDD
    val columnNames = "rowkey" +: rowRDD.groupBy(x => x._2).map(x => x._1).sortBy(x => x).collect()

    val bColumnNames = sc.broadcast(columnNames)

    // prepare fullRowRDD from rowRDD
    val fullRowRDD = rowRDD.groupBy(x => x._1).map(data => {
      // prepare column Map (column name, value)
      val colMap = data._2.map(t => (t._2, t._3)).toMap

      // get the column Names
      val colNames = bColumnNames.value

      // prepare full row data
      val rowData = colNames.map { colName =>
        // convert PhoenixType To CatalystType
        val dataType = if (schemaMapping != null) {
          phoenixTypeToCatalystType(schemaMapping.get(colName).getOrElse("PVarchar"))
        } else {
          phoenixTypeToCatalystType("PVarchar")
        }

        val pdata = if ("rowkey".equals(colName)) data._1 else colMap.getOrElse(colName, "")

        // convert to Spark Catalyst type
        convertToCatalystType(pdata, dataType)
      }

      // prepare row from rowData
      Row.fromSeq(rowData)
    })

    fullRowRDD.foreach { row => println(row) }

    // create Schema from fields
    val schema = getSchema(columnNames, fieldsMapping, schemaMapping)

    // Create DataFrame from fullRowRDD and Schema
    sqlContext.createDataFrame(fullRowRDD, schema)
  }

  def getSchema(columnNames: Array[String], fieldsMapping: Map[String, String], schemaMapping: Map[String, String]): StructType = {
    // get the fields with proper type
    val fields =
      if (fieldsMapping == null) {
        columnNames.map { columnName => StructField(columnName, StringType) }.toArray
      } else {
        if (schemaMapping == null) {
          fieldsMapping.values.map { columnName => StructField(columnName, StringType) }.toArray
        } else {
          schemaMapping.map(x => StructField(x._1, phoenixTypeToCatalystType(x._2))).toArray
        }
      }

    // create Schema from fields
    val schema = StructType(fields)
    schema
  }

  // prepare tuple with (rowkey, columnfamily:qualifier, value)
  def convertToRow(data: Iterator[(ImmutableBytesWritable, KeyValue)]): Iterator[(String, String, String)] = {
    data.map(x => {
      val keyvalue = x._2
      val rowkey = Bytes.toStringBinary(keyvalue.getRow())
      val family = Bytes.toStringBinary(keyvalue.getFamily())
      val qualifier = Bytes.toStringBinary(keyvalue.getQualifier())
      val value = Bytes.toStringBinary(keyvalue.getValue)
      val column = family + ":" + qualifier
      (rowkey, column, value)
    })
  }

  def convertToCatalystType(data: String, dataType: DataType): Any = dataType match {
    case t if t.isInstanceOf[LongType] => Long.parseLong(data)
    case t if t.isInstanceOf[IntegerType] => Integer.parseInt(data)
    case t if t.isInstanceOf[ShortType] => Short.parseShort(data)
    case t if t.isInstanceOf[FloatType] => Float.parseFloat(data)
    case t if t.isInstanceOf[DoubleType] => Double.parseDouble(data)
    case t if t.isInstanceOf[BooleanType] => Boolean.parseBoolean(data)
    case t if t.isInstanceOf[TimestampType] => Timestamp.valueOf(data)
    case t if t.isInstanceOf[DateType] => Date.valueOf(data)
    case t if t.isInstanceOf[StringType] => data
    case t if t.isInstanceOf[ByteType] => Bytes.toBytes(data)
  }

  // Lookup table for Phoenix types to Spark catalyst types
  // TODO: PDecimal, PDecimalArray are not supported
  def phoenixTypeToCatalystType(columnInfo: String): DataType = columnInfo match {
    case t if t.equalsIgnoreCase("PVarchar") || t.equalsIgnoreCase("PChar") => StringType
    case t if t.equalsIgnoreCase("PLong") || t.equalsIgnoreCase("PUnsignedLong") => LongType
    case t if t.equalsIgnoreCase("PInteger") || t.equalsIgnoreCase("PUnsignedInt") => IntegerType
    case t if t.equalsIgnoreCase("PSmallint") || t.equalsIgnoreCase("PUnsignedSmallint") => ShortType
    case t if t.equalsIgnoreCase("PTinyint") || t.equalsIgnoreCase("PUnsignedTinyint") => ByteType
    case t if t.equalsIgnoreCase("PFloat") || t.equalsIgnoreCase("PUnsignedFloat") => FloatType
    case t if t.equalsIgnoreCase("PDouble") || t.equalsIgnoreCase("PUnsignedDouble") => DoubleType
    case t if t.equalsIgnoreCase("PTimestamp") || t.equalsIgnoreCase("PUnsignedTimestamp") => TimestampType
    case t if t.equalsIgnoreCase("PTime") || t.equalsIgnoreCase("PUnsignedTime") => TimestampType
    case t if t.equalsIgnoreCase("PDate") || t.equalsIgnoreCase("PUnsignedDate") => DateType
    case t if t.equalsIgnoreCase("PBoolean") => BooleanType
    case t if t.equalsIgnoreCase("PVarbinary") || t.equalsIgnoreCase("PBinary") => BinaryType
    case t if t.equalsIgnoreCase("PIntegerArray") || t.equalsIgnoreCase("PUnsignedIntArray") => ArrayType(IntegerType, containsNull = true)
    case t if t.equalsIgnoreCase("PBooleanArray") => ArrayType(BooleanType, containsNull = true)
    case t if t.equalsIgnoreCase("PVarcharArray") || t.equalsIgnoreCase("PCharArray") => ArrayType(StringType, containsNull = true)
    case t if t.equalsIgnoreCase("PVarbinaryArray") || t.equalsIgnoreCase("PBinaryArray") => ArrayType(BinaryType, containsNull = true)
    case t if t.equalsIgnoreCase("PLongArray") || t.equalsIgnoreCase("PUnsignedLongArray") => ArrayType(LongType, containsNull = true)
    case t if t.equalsIgnoreCase("PSmallintArray") || t.equalsIgnoreCase("PUnsignedSmallintArray") => ArrayType(IntegerType, containsNull = true)
    case t if t.equalsIgnoreCase("PTinyintArray") || t.equalsIgnoreCase("PUnsignedTinyintArray") => ArrayType(ByteType, containsNull = true)
    case t if t.equalsIgnoreCase("PFloatArray") || t.equalsIgnoreCase("PUnsignedFloatArray") => ArrayType(FloatType, containsNull = true)
    case t if t.equalsIgnoreCase("PDoubleArray") || t.equalsIgnoreCase("PUnsignedDoubleArray") => ArrayType(DoubleType, containsNull = true)
    case t if t.equalsIgnoreCase("PTimestampArray") || t.equalsIgnoreCase("PUnsignedTimestampArray") => ArrayType(TimestampType, containsNull = true)
    case t if t.equalsIgnoreCase("PDateArray") || t.equalsIgnoreCase("PUnsignedDateArray") => ArrayType(TimestampType, containsNull = true)
    case t if t.equalsIgnoreCase("PTimeArray") || t.equalsIgnoreCase("PUnsignedTimeArray") => ArrayType(TimestampType, containsNull = true)
  }
}